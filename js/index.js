$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 1000 /*rota cada 1 segundo*/
    });
    $('#suscribete').on('show.bs.modal', function(e){
        console.log('el modal se esta mostrando');
        $('#suscribeteBtn').removeClass('btn-warning');
        console.log('se removio la clase btn-warning');
        $('#suscribeteBtn').addClass('btn-primary');
        console.log('se agrego otra clase btn-primary');
        $('#suscribeteBtn').prop('disabled', true);
        console.log('se desactivo el boton');
    });
    $('#suscribete').on('shown.bs.modal', function(e){
        console.log('el modal se mostro');
    });
    $('#suscribete').on('hide.bs.modal', function(e){
        console.log('el modal se esta ocultando');
    });
    $('#suscribete').on('hidden.bs.modal', function(e){
        console.log('el modal se oculto');
        $('#suscribeteBtn').prop('disabled', false);
        console.log('se activo el boton');
        $('#suscribeteBtn').removeClass('btn-primary');
        console.log('se removio la clase btn-primary');
        $('#suscribeteBtn').addClass('btn-warning');
        console.log('se agrego otra clase btn-warning');
    });
});